# Wordpress Theme: [Sage](https://roots.io/sage/)
[![Packagist](https://img.shields.io/packagist/vpre/roots/sage.svg?style=flat-square)](https://packagist.org/packages/roots/sage)
[![devDependency Status](https://img.shields.io/david/dev/roots/sage.svg?style=flat-square)](https://david-dm.org/roots/sage#info=devDependencies)
[![Build Status](https://img.shields.io/travis/roots/sage.svg?style=flat-square)](https://travis-ci.org/roots/sage)

# Тестовое задание "Слайдер"
* **Исполнитель:** Оганесов Левон
* **Email:** [loganesov@3atdev.com](mailto:loganesov@3atdev.com)

---

### 1. Установка локально
1. Установить: [Wordpress](https://ru.wordpress.org/download/ "Перейти на страницу скачивания")
2. Установить плагин: [ACF (Advanced Custom Fields)](https://wordpress.org/plugins/advanced-custom-fields/ "Перейти на страницу скачивания")
3. В админке перейти в раздел Плагины и активировать ACF (Advanced Custom Fields)
4. Перейти в папку **```wp-content/themes```**, выполнить в папке клонирование темы **```git clone git@bitbucket.org:loganesov/wp_test.git```**
5. В админке перейти в раздел **```Внешний вид -> Темы```** и активировать тему **```wp_sage_test```**
6. Перейти в **```Настройки -> чтения```** выбрать доступную статическую страницу
7. В папке с темой выполнить **```sudo yarn```**
8. После успешной установки 1.7 пункта, выполнить **```yarn run start```** или **```yarn run build```**

### 2. Создание слайдера на странице
1. Перейти в раздел **```Страницы```**
2. Зайти в страницу которая указана в пункте **1.6**
3. В низу будет раздел **```Слайдер```**, в котором можно добавить слайды.
    + Список полей слайда
        * Изображение: ```return: Array | type: media | name: image | required: true```
        * Заголовок: ```return: String | type: text | name: title | required: false```
        * Текст: ```return: String | type: textarea | name: description | required: false```
        * Ссылка: ```return: String | type: url | name: link | required: false```
4. После добавление слайдов, сохранить страницу.
5. Результат можно увидеть перейдя на страницу.
