@if($images)
  <div class="slider">
    <div class="shows">
      <div class="swiper-container">
        <div class="swiper-wrapper">

          <!-- Slides -->
          @foreach($images as $item)
            <div class="swiper-slide">
              <article class="show">
                <div class="title">{{$item['title']}}</div>
                <div class="description description-devices">{{$item['description']}}</div>
                <a
                  href="{{$item['link']}}"
                  title="{{$item['title']}}"
                  class="wrapper"
                >
                  <img
                    data-src="{{$item['image']['sizes']['custom-image-600-300']}}"
                    alt="{{$item['title']}}"
                    class="swiper-lazy"
                  />
                  <div class="swiper-lazy-preloader">
                    <div class="d-flex justify-content-center align-items-center">
                      <div class="spinner spinner-border" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                    </div>
                  </div>
                </a>
                <div class="description description-desktop">{{$item['description']}}</div>
              </article>
            </div>
          @endforeach

        </div>
      </div>
      <div class="shows-button-prev"><span></span></div>
      <div class="shows-button-next"><span></span></div>
    </div>
  </div>
@endif
